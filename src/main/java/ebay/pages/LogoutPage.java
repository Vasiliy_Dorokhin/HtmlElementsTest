package ebay.pages;

import ebay.blocks.HeaderPanel;
import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface LogoutPage extends WebPage {

    @Description("Message {{ text }}")
    @FindBy("//span[text()='{{ text }}']")
    ExtendedWebElement messege(@Param("text") String text);

    @Description("Header panel")
    @FindBy("//header[@id='gh']")
    HeaderPanel headerPanel();

}
