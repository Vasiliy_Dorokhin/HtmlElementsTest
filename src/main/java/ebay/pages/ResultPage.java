package ebay.pages;

import ebay.blocks.SearchResultItem;
import ebay.blocks.UserMenu;
import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;

public interface ResultPage extends WebPage {

    @Description("List of phones")
    @FindBy("//ul[@id='ListViewInner']//li[contains(@id, 'item')]")
    ExtendedList<SearchResultItem> phoneList();

    @Description("User menu")
    @FindBy("//div[@id='gh-top']")
    UserMenu userMenu();

}
