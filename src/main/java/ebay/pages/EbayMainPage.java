package ebay.pages;

import ebay.blocks.HeaderPanel;
import ebay.blocks.UserMenu;
import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.extension.page.BaseUrl;

@BaseUrl("https://www.ebay.com/")
public interface EbayMainPage extends WebPage {

    @Description("Header panel")
    @FindBy("//header[@id='gh']")
    HeaderPanel headerPanel();

    @Description("User menu")
    @FindBy("//div[@id='gh-top']")
    UserMenu userMenu();

}
