package ebay.pages;

import ebay.blocks.LoginForm;
import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface AuthorizationPage extends WebPage {

    @Description("Login form")
    @FindBy("//div[div[@id='mainCnt']]")
    LoginForm loginForm();

}
