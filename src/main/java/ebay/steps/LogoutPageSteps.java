package ebay.steps;

import ebay.pages.LogoutPage;
import io.qameta.htmlelements.WebPageFactory;
import org.openqa.selenium.WebDriver;

import static io.qameta.htmlelements.matcher.DisplayedMatcher.displayed;

public class LogoutPageSteps {

    private LogoutPage logoutPage;

    public LogoutPageSteps(WebDriver webDriver){
        WebPageFactory factory = new WebPageFactory();
        logoutPage = factory.get(webDriver, LogoutPage.class);
    }

    public LogoutPageSteps checkLogoutMessageText(String text){
        logoutPage.messege(text).should(displayed());
        return this;
    }

    public void clickOnLogo() {
        logoutPage.headerPanel().logoBtn().should(displayed()).click();
    }

}
