package ebay.steps;

import ebay.pages.EbayMainPage;
import ebay.webElements.MineWebElements;
import io.qameta.htmlelements.WebPageFactory;
import org.openqa.selenium.WebDriver;

import static io.qameta.htmlelements.matcher.DisplayedMatcher.displayed;

public class MainEbayPageSteps {

    private EbayMainPage ebayMainPage;

    public MainEbayPageSteps(WebDriver webDriver){
        WebPageFactory factory = new WebPageFactory();
        ebayMainPage = factory.get(webDriver, EbayMainPage.class);
    }

    public MainEbayPageSteps openEbay(){
        ebayMainPage.go();
        return this;
    }

    public void clickToSigninButton(){
        ebayMainPage.userMenu().signInBtn().waitUntil(displayed()).click();
    }

    public MainEbayPageSteps inputInSearchField(String text) {
        MineWebElements input = ebayMainPage.headerPanel().search();
        input.waitUntil(displayed()).clear();
        input.sendKeys(text);
        return this;
    }

    public void clickOnFindButton() {
        ebayMainPage.headerPanel().findBtn().waitUntil(displayed()).click();
    }

    public MainEbayPageSteps checkLogInButtonIsDisplayed(){
        ebayMainPage.userMenu().signInBtn().should(displayed());
        System.out.println("Everything cool!");
        return this;
    }


}
