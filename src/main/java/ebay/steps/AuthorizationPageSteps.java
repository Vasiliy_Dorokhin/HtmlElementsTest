package ebay.steps;

import ebay.pages.AuthorizationPage;
import ebay.webElements.MineWebElements;
import io.qameta.htmlelements.WebPageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static io.qameta.htmlelements.matcher.DisplayedMatcher.displayed;

public class AuthorizationPageSteps {

    private AuthorizationPage authorizationPage;

    public AuthorizationPageSteps(WebDriver webDriver){
        WebPageFactory factory = new WebPageFactory();
        authorizationPage = factory.get(webDriver, AuthorizationPage.class);
    }

    public AuthorizationPageSteps inputUsername(String username) {
        MineWebElements input = authorizationPage.loginForm().usernameField();
        input.waitUntil(displayed()).clear();
        input.sendKeys(username);
        return this;
    }

    public AuthorizationPageSteps inputPassword(String password) {
        MineWebElements input = authorizationPage.loginForm().passwordField();
        input.waitUntil(displayed()).clear();
        input.sendKeys(password);
        return this;
    }

    public void clickOnSignInButton(){
        authorizationPage.loginForm().signInBtn().waitUntil(displayed()).click();
    }

}
