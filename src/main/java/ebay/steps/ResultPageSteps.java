package ebay.steps;

import ebay.blocks.SearchResultItem;
import ebay.pages.ResultPage;
import io.qameta.htmlelements.WebPageFactory;
import io.qameta.htmlelements.element.ExtendedList;
import org.openqa.selenium.WebDriver;

import static io.qameta.htmlelements.matcher.DisplayedMatcher.displayed;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.MatcherAssert.assertThat;

public class ResultPageSteps {

    private ResultPage resultPage;

    public ResultPageSteps(WebDriver webDriver){
        WebPageFactory factory = new WebPageFactory();
        resultPage = factory.get(webDriver, ResultPage.class);
    }

    public ResultPageSteps checkItemListSize(int num){
        ExtendedList<SearchResultItem> expectedList = resultPage.phoneList();
        expectedList.should(everyItem(displayed()));
        assertThat(String.format("The number of elements is different from [%s]", num),
                expectedList.size(), equalTo(num));
        return this;
    }

    public void clickOnLogOutButton(){
        resultPage.userMenu().userSettingsBtn().should(displayed()).click();
        resultPage.userMenu().logOutBtn().should(displayed()).click();
    }

}
