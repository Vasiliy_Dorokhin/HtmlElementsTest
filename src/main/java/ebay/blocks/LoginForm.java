package ebay.blocks;

import ebay.webElements.MineWebElements;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface LoginForm extends ExtendedWebElement<LoginForm> {

    @Description("Username")
    @FindBy("//input[@id='userid']")
    MineWebElements usernameField();

    @Description("Password")
    @FindBy("//input[@id='pass']")
    MineWebElements passwordField();

    @Description("SignIn button")
    @FindBy("//input[@id='sgnBt']")
    MineWebElements signInBtn();


}
