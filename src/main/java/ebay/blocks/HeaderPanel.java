package ebay.blocks;

import ebay.webElements.MineWebElements;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface HeaderPanel extends ExtendedWebElement<HeaderPanel> {

    @Description("Logo")
    @FindBy("//a[child::img[@id='gh-logo']]")
    MineWebElements logoBtn();

    @Description("Search field")
    @FindBy("//input[contains(@class, 'ui-autocomplete-input')]")
    MineWebElements search();

    @Description("Find button")
    @FindBy("//input[@id='gh-btn']")
    MineWebElements findBtn();

}
