package ebay.blocks;

import ebay.steps.MainEbayPageSteps;
import ebay.webElements.MineWebElements;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import org.openqa.selenium.WebElement;

public interface UserMenu extends ExtendedWebElement<UserMenu> {

    @Description("SignIn button")
    @FindBy("//a[text()='Войдите']")
    MineWebElements signInBtn();

    @Description("User settings button")
    @FindBy("//button[@id='gh-ug']")
    MineWebElements userSettingsBtn();

    @Description("LogOut button")
    @FindBy("//a[text()='Выход']")
    MineWebElements logOutBtn();

}
