package ebay.blocks;

import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;

public interface SearchResultItem extends ExtendedWebElement<SearchResultItem> {

    @Description("Title")
    @FindBy("//h3[@class='lvtitle']/a")
    ExtendedWebElement resultTitle();

}
