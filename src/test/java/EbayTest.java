import ebay.steps.AuthorizationPageSteps;
import ebay.steps.LogoutPageSteps;
import ebay.steps.MainEbayPageSteps;
import ebay.steps.ResultPageSteps;
import io.qameta.htmlelements.annotation.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class EbayTest {

    private static WebDriver webDriver;
    private MainEbayPageSteps mainEbayPageSteps;
    private AuthorizationPageSteps authorizationPageSteps;
    private ResultPageSteps resultPageSteps;
    private LogoutPageSteps logoutPageSteps;
    private String login;
    private String password;
    private String phone;


    @Before
    public void setupTest() {
        webDriver = new ChromeDriver();
        mainEbayPageSteps = new MainEbayPageSteps(webDriver);
        authorizationPageSteps = new AuthorizationPageSteps(webDriver);
        resultPageSteps = new ResultPageSteps(webDriver);
        logoutPageSteps = new LogoutPageSteps(webDriver);
        login = "TestForHtmlElements";
        password = "Qwerty123456";
        phone = "blackberry";
    }

    @Test
    @Description("Checking items list size and logout")
    public void openTest() {
        mainEbayPageSteps
                .openEbay()
                .clickToSigninButton();
        authorizationPageSteps
                .inputUsername(login)
                .inputPassword(password)
                .clickOnSignInButton();
        mainEbayPageSteps
                .inputInSearchField(phone)
                .clickOnFindButton();
        resultPageSteps
                .checkItemListSize(50)
                .clickOnLogOutButton();
        logoutPageSteps
                .checkLogoutMessageText("Выход успешно выполнен. До скорой встречи.")
                .clickOnLogo();
        mainEbayPageSteps
                .checkLogInButtonIsDisplayed();
    }

    @After
    public void close(){
        if(webDriver != null) {
            webDriver.close();
        }
    }

}
